var express = require('express'),
    path = require('path'),
    favicon = require('serve-favicon'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    mongojs = require('mongojs');
var main     = express();

var db = mongojs('52.37.247.194:7890/altizon', ['thing_history']);
mongoose.connect('mongodb://52.37.247.194:7890/altizon');

var routes = require('./routes/api.js');

main.use(logger('dev'));
main.use(bodyParser.json());
main.use(bodyParser.urlencoded({ extended: false }));
main.use(cookieParser());

main.use(express.static(path.join(__dirname, '../public')));

main.use('/user/', routes);

main.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, '../public', 'views/index.html'));
});

main.get('/upsUtilization',function(req,res) {
	  db.thing_history.aggregate([
        { $group: { _id: "$_d", battModeRT: { $sum: "$data.battModeRT" }, battEnergy: { $sum: "$data.battEnergy" } } },
        { $project: { _id: 1, battEnergy: 1, battModeRT: { $multiply: [ "$battModeRT", { $divide: [ 100, 86400000 ] }] } } },
        {$sort :{_id:1}}
    ], function(err, data){
        if(err)
          res.send(err);
        res.send(data);
    });
});


main.all('/*', function(req, res, next) {
  // CORS headers
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});

// error handlers

// development error handler
// will print stacktrace
if (main.get('env') === 'development') {
  main.use(function(err, req, res, next) {
    console.log(err);
    res.status(err.status || 500);
    //res.render('error', {
    //  message: err.message,
    //  error: err
    //});
  });
}

// production error handler
// no stacktraces leaked to user
main.use(function(err, req, res, next) {
  console.log(err);
  res.status(err.status || 500);
  /*res.render('error', {
    message: err.message,
    error: {}
  }); */
});

module.exports = main;
