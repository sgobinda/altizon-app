var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var alertSchema = new Schema({
  user: { type: String, required: true },
  token: { type: String, required: false },
  lastCall: { type: Date, required: false }
});

// the schema is useless so far
// we need to create a model using it
var Alert = mongoose.model('Alert', alertSchema);

// make this available to our users in our Node applications
module.exports = Alert;
