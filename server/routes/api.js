var express = require('express'),
router = express.Router(),
request = require('request'),
Client = require('node-rest-client').Client;
Alert = require('../model/alertModel.js');
var client = new Client();

router.post('/loginToAltizon', function(req, res, next) {

  var email = req.body.userName;

  var args = {
    data: {
      "email":email,
      "password":req.body.password
    },
    headers: { "Content-Type": "application/json" }
  };

  var loginReq = client.post("https://api.datonis.io/api_sign_in", args, function (data, response) {

    if(data.auth_token !== undefined) {

      Alert.findOne({user: email}, function(err, docs) {
        console.log(docs);
        if(docs) {
          var ids = docs._id;
          console.log(ids);
          if(ids !== undefined && ids !== null && ids !== '') {
            Alert.findByIdAndUpdate({_id: ids}, {$set: {token: data.auth_token}}, function (err, resAlert) {
              if(err) {
                console.error(err);
              }
              console.log('token Updated..');
              console.log(resAlert);
            });
          }
        }else {
          var alert = new Alert({
            user: email,
            token: data.auth_token,
            lastCall: ''
          });
          alert.save(function(err, alert) {
            if (err){
              console.error(err);
            }
            console.log(alert);
          });
        }
      });
      return res.json({token: data.auth_token});
    }else {
      return res.sendStatus(401);
    }
  });

  loginReq.on('requestTimeout', function (req) {
    console.log("request has expired");
    req.abort();

  });

  loginReq.on('responseTimeout', function (res) {
    console.log("response has expired");

  });

  loginReq.on('error', function (err) {
    console.error('something went wrong on request!!', err);
  });

});

router.get('/thing/getAllThings', function(req, res, next) {


  request({
    uri: "https://api.datonis.io/api/v3/things",
    headers: {
      'Content-Type': 'application/json',
      'X-Auth-Token': req.headers.authorization
    }
  }, function(error, response, body) {
    if(error){
      console.error(error);
      return res.send(error);
    }
    return res.send(body);
  });

});

router.get('/thing/getThingDetails/:thingKey', function(req, res, next) {

  request({
    uri: "https://api.datonis.io/api/v3/things/"+req.params.thingKey,
    headers: {
      'Content-Type': 'application/json',
      'X-Auth-Token': req.headers.authorization
    }
  }, function(error, response, body) {
    if(error) {
      console.log(error);
      return res.send(error);
    }
    return res.send(body);
  });

});

router.post('/thing/queryThingData', function(req, res, next) {
  var args = {
    data:  {
      "thing_key": req.body.thing_key,
      "from":req.body.from,
      "to":req.body.to,
      "time_zone":req.body.time_zone,
      "time_format":req.body.time_format,
      "per":req.body.per
    },
    headers: {
      'Content-Type': 'application/json',
      'X-Auth-Token': req.headers.authorization
    }
  };

  var queryData = client.post("https://api.datonis.io/api/v3/datonis_query/thing_data", args, function (data, response) {
    return res.send(data);
  });

  queryData.on('requestTimeout', function (req) {
    console.error("request has expired");
    req.abort();
  });

  queryData.on('responseTimeout', function (res) {
    console.error("response has expired");
  });

  queryData.on('error', function (err) {
    console.error('something went wrong on request!!', err.request.options);
    return res.send(err);
  });


});

router.post('/getThingTickets', function(req, res) {
  var searchKey = req.body.name,
  path = 'http://52.37.247.194/webservices/rest.php?' +
  'json_data={' +
  encodeURIComponent('"operation"') + ':' + encodeURIComponent('"core')+ '/' + encodeURIComponent('get"') + ',' +
  encodeURIComponent('"class"') + ':' + encodeURIComponent('"Incident"') + ',' +
  encodeURIComponent('"output_fields"') + ':' + encodeURIComponent('"team_id_friendlyname') + ',' +
  encodeURIComponent('agent_id_friendlyname') + ',' + encodeURIComponent('friendlyname') + ',' +
  encodeURIComponent('start_date') + ',' + encodeURIComponent('title') + ',' +
  encodeURIComponent('status') + ',' + encodeURIComponent('priority') + ',' +
  encodeURIComponent('description"') + ',' +
  encodeURIComponent('"key"') + ':' + encodeURIComponent('"SELECT Incident WHERE caller_id_friendlyname LIKE \'') + searchKey + encodeURIComponent('%\'"')
  + '}&version=1.0&auth_user=wipro&auth_pwd=wipro@123';
  // console.log(path);
  var args = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  var queryData = client.post(path, args, function (data, response) {
    // consoles.log(data);
    return res.send(data);
  });
  queryData.on('error', function (err) {
    console.error(err);
    return res.send(err);
  });
});


router.post('/getAllTickets', function(req, res) {
  var searchKey = req.body.name,
  path = 'http://52.37.247.194/webservices/rest.php?' +
  'json_data={' +
  encodeURIComponent('"operation"') + ':' + encodeURIComponent('"core')+ '/' + encodeURIComponent('get"') + ',' +
  encodeURIComponent('"class"') + ':' + encodeURIComponent('"Incident"') + ',' +
  encodeURIComponent('"output_fields"') + ':' + encodeURIComponent('"team_id_friendlyname') + ',' +
  encodeURIComponent('agent_id_friendlyname') + ',' + encodeURIComponent('friendlyname') + ',' +
  encodeURIComponent('start_date') + ',' + encodeURIComponent('title') + ',' +
  encodeURIComponent('status') + ',' + encodeURIComponent('priority') + ',' +
  encodeURIComponent('description"') + ',' +
  encodeURIComponent('"key"') + ':' + encodeURIComponent('"SELECT Incident"')
  + '}&version=1.0&auth_user=wipro&auth_pwd=wipro@123';
  // console.log(path);
  var args = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  var queryData = client.post(path, args, function (data, response) {
    // consoles.log(data);
    return res.send(data);
  });
  queryData.on('error', function (err) {
    return res.send(err);
  });
});

router.get('/thing/getAllAlerts', function(req, res, next) {

  var token = req.headers.authorization;

  var args = {
    parameters: {
      "order_by":"created_at",
      "order":"desc",
      "timestamp_format":"str",
      "per" : req.query.per,
      "page" : req.query.pageNumber
    }, // query parameter substitution vars
    headers: {
      'Content-Type': 'application/json',
      'X-Auth-Token': token
    } // request headers
  };

  var request = client.get("https://api.datonis.io/api/v3/alerts", args,
  function (data, response) {
    console.log(data);
    Alert.findOne({token: token}, function(err, results) {
      if(results) {
        var ids = results._id;
        console.log('ids===='+ids);
        Alert.findByIdAndUpdate({_id: ids}, {$set: {lastCall: new Date(data.alerts[0].created_at)}}, function (err, resAlert) {
          if(err) {
            console.error(err);
          }
          console.log('Updated last alert time..');
          console.log(resAlert);
          return res.json({alertData: data});
        });
      }
    });

  });
  request.on('error', function (err) {
    console.error(err);
    return res.send(err);
  });
});

router.get('/logoutFromAltizon', function(req, res, next) {

  console.log(req.headers);
  return res.sendStatus(200);
});


router.get('/thing/getAlertNotifications', function(req, res, next) {

  var token = req.headers.authorization;

  var args = {
    parameters: {
      "order_by":"created_at",
      "order":"desc",
      "timestamp_format":"str",
      "per" : "50",
      "page" : "1"
    }, // query parameter substitution vars
    headers: {
      'Content-Type': 'application/json',
      'X-Auth-Token': token
    } // request headers
  };

  var request = client.get("https://api.datonis.io/api/v3/alerts", args,
  function (data, response) {

    var messages = [];
    Alert.findOne({token: token}, function(err, results) {
      //console.log(results);
      if(results) {
        console.log('lastAlertTime===='+results.lastCall);
        //console.log(data.alerts);
        if(data.alerts){
          var alerts = data.alerts;
          for(var alert in alerts) {
            if(new Date(alerts[alert].created_at).valueOf() > new Date(results.lastCall).valueOf()){
              console.log(alerts[alert].message);
              messages.push(alerts[alert].message);
            }
          }
        }
      }
      console.log(messages);
      return res.json({messages: messages});
    });
  });
  
  request.on('error', function (err) {
    console.error(err);
    return res.send(err);
  });
});
module.exports = router;
