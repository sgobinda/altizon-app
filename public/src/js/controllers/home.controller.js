
angular.module('altizonApp').controller('homeCtrl', function($scope, $state, $interval, $filter, LogoutService, $window, AuthenticationService, DashboardService) {

  $scope.messages = [];
  $scope.isShowing = false;
  $scope.showCountInNotification = $scope.messages.length;

  $scope.active = ['active', '', ''];

  $scope.logOutUser = function() {
    if (AuthenticationService.isAuthenticated) {
      LogoutService.logout().success(function(data) {
          AuthenticationService.isAuthenticated = false;
          delete $window.sessionStorage.token;
          $state.go("login");
      }).error(function(status, data) {
          console.log(status);
          console.log(data);
      });
    }
    else {
        $state.go("login");
    }
  };

  $scope.updateActiveTab = function(tabindex) {
    $scope.active = ['','',''];
    $scope.active[tabindex] = 'active';
  };

  // This space used for creating variables which can be accessed in any state
  $scope.sharedData = {
    updateActiveTab: $scope.updateActiveTab,
    thingsToHide: ['ViscocitySensor', 'Heat', 'UPS_CLONE']
  };

  $scope.goAlertsPage = function() {
    $scope.isShowing = false;
    $state.go("home.alerts");
  };

  $scope.getAlertsForNotification = function() {
    DashboardService.getAlertNotifications().then(function (response) {
      console.log(response.data);
      var alertMessages = response.data.messages;
      console.log(alertMessages);
      if(alertMessages.length > 0){
        $scope.messages = alertMessages;
        $scope.isShowing = true;
        $scope.showCountInNotification = alertMessages.length;
      }else {
        $scope.isShowing = false;
        $scope.messages = [];
        $scope.showCountInNotification = 0;
      }
    });

  };

  //$scope.getAlertsForNotification();

  if (AuthenticationService.isAuthenticated) {
    $interval($scope.getAlertsForNotification, 60000);
  }

});
