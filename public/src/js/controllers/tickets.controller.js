angular.module('altizonApp').controller('TicketsCtrl', ['$scope', 'Ticketservice', 'DashboardService', '$q',
function ($scope, Ticketservice, DashboardService, $q) {
  $scope.sharedData.updateActiveTab(2); // updating active tab

  $scope.changeTickets = function(thingName) {
    $scope.currentThingName = thingName;

    $scope.isProcessing = true;
    Ticketservice.queryThingTickets({name : $scope.currentThingName}).success(
      function(data) {
        //console.log(data);
        var tickets = data.objects,
            ind = 0;
        $scope.sharedData.tickets = [];
        for(var ticket in tickets) {
          var tickObj = tickets['' + ticket].fields,
              priority = ['critical', 'high', 'medium', 'low'],
              assignedTo = tickObj.team_id_friendlyname + '/' + tickObj.agent_id_friendlyname;
          $scope.sharedData.tickets[ind] = {
            ticketNo : tickObj.friendlyname,
            sensor : tickObj.title,
            createdTime : tickObj.start_date,
            assignedTo : assignedTo.length > 2? assignedTo: '',
            status: tickObj.status,
            severity: priority[tickObj.priority-1],
            message: tickObj.description,
            parameter: ""
          };
          ind++;
        }
        $scope.sharedData.tickets.sort(function(a, b) {
          return Date.parse(b.createdTime) - Date.parse(a.createdTime);
        });
        $scope.displayTickets = 5;
        $scope.gridOptions.data = $scope.sharedData.tickets.slice(0,$scope.displayTickets);
        $scope.isProcessing = false;
      }
    );
  };
  $scope.loadTickets = function() {
    $scope.displayTickets+=5;
    if($scope.sharedData.tickets.length >= $scope.displayTickets)
      $scope.gridOptions.data = $scope.sharedData.tickets.slice(0,$scope.displayTickets);
    else
      $scope.gridOptions.data = $scope.sharedData.tickets;
  };
  $scope.getDataDown = function() {
    var promise = $q.defer();
    $scope.loadTickets();
    $scope.gridApi.infiniteScroll.saveScrollPercentage();
    $scope.gridApi.infiniteScroll.dataLoaded().then(function() {
        promise.resolve();
    });
    return promise.promise;
  };

  if($scope.sharedData.tickets === undefined)
    $scope.changeTickets('All Tickets');

  if($scope.sharedData.things === undefined) {
    DashboardService.getAllThings().then(function (response) {
      $scope.sharedData.things = response.data.things;
      thingsNameArr = $scope.sharedData.things.map(function(d) {
        return d.name;
      });
      var thingsToHide = $scope.sharedData.thingsToHide;
      for(var i in thingsToHide) {
          $scope.sharedData.things.splice(thingsNameArr.indexOf(thingsToHide[i])-i,1);
      }
    });
  }
  $scope.displayTickets = 5;
  $scope.currentThingName = $scope.sharedData.currentThingName? $scope.sharedData.currentThingName: 'All Tickets';
  $scope.gridOptions = {
    enableSorting: true,
    enableVerticalScrollbar: 1,
    columnDefs: [
      { field : 'ticketNo', width: '10%' },
      { field : 'sensor', enableColumnResizing: true, width: '25%' },
      { field : 'createdTime', width: '13%' },
      { field : 'assignedTo', enableColumnResizing: true, width: '10%' },
      { field : 'status', width: '5%' },
      { field : 'severity', width: '5%'},
      { field : 'message', enableColumnResizing: true, width: '22%' },
      { field : 'parameter', enableColumnResizing: true,  width: '10%' }
    ],
    data: $scope.sharedData.tickets === undefined? []:$scope.sharedData.tickets.slice(0,$scope.displayTickets),
    enableHorizontalScrollbar:  0,
    onRegisterApi: function(gridApi){

      gridApi.infiniteScroll.on.needLoadMoreData($scope, $scope.getDataDown);
      $scope.gridApi = gridApi;
    }
  };
}]);
