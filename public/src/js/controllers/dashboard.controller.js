
angular.module('altizonApp').controller('DashboardCtrl', function ($scope, DashboardService, getThings, $filter, $q, Ticketservice) {
  $scope.sharedData.updateActiveTab(0); // setting active tab in nav bar
  $scope.isProcessing = true;
  thingsNameArr = getThings.things.map(function(d) {
    return d.name;
  });

  thingsToHide = $scope.sharedData.thingsToHide;
  $scope.things = getThings.things;
  // $scope.things[thingsNameArr.indexOf("UPS")].thing_key = $scope.things[thingsNameArr.indexOf("UPS_CLONE")].thing_key;
  console.log($scope.things);
  for(var i in thingsToHide) {
      $scope.things.splice(thingsNameArr.indexOf(thingsToHide[i])-i,1);
  }
  $scope.things[thingsNameArr.indexOf('UPS')-2].metrics.splice(6,2);
  $scope.isProcessing = false;
  $scope.sharedData.things = $scope.things;
  $scope.metricLabels = {};
  $scope.overviewMetricDisplay = ['thingLatesteventData col-md-3','thingLatesteventData col-md-3','thingLatesteventData col-md-6'];
  //abstract Thing ticket informtion.
  $scope.getThingDetails = function (thingKey) {
    return DashboardService.getThingDetails(thingKey).then(function (response) {
      //console.log(response.data.thing.user_defined_properties);
      $scope.thingData = response.data.thing;
      console.log($scope.thingData);
    });
  };
  $scope.gridOptions = {
    enableSorting: true,
    enableVerticalScrollbar: 0,
    columnDefs: [
      { field : 'ticketNo', width: '20%' },
      { field : 'sensor', enableColumnResizing: true, width: '30%' },
      { field : 'createdTime', width: '30%' },
      { field : 'assignedTo', enableColumnResizing: true, visible: false },
      { field : 'status', width: '20%' },
      { field : 'severity', visible: false },
      { field : 'message', enableColumnResizing: true, visible: false },
      { field : 'parameter', enableColumnResizing: true, visible: false }

    ],
    enableHorizontalScrollbar:  0
  };
  $scope.tickets = [];
  $scope.getCurrentDate = function(tdate) {
    return $filter('date')(tdate, 'yyyy/MM/dd HH:mm:ss');
  };

  $scope.getYesterdayDate = function(pdate) {
    return $filter('date')((pdate.setDate(pdate.getDate() - 1)), 'yyyy/MM/dd HH:mm:ss');
  };
  function generateBarChartData(object, thing) {
    console.log(thing);
    for(var j=0;j<object.length; j++) {
      object[j]._id = new Date(Date.parse(object[j]._id));
    }
    thing.graphData.dataset1= object;
    var parameters = [];
    for(var key in object[0]) {
      parameters.push(key);
    }
    console.log(parameters);
    // $scope.graphOption = [];
    $scope.metricLabels.battEnergy = "Battery Energy Consumption (kWh)";
    $scope.metricLabels.battModeRT = "Utilization (%)";
    for(var i=1,len=parameters.length; i<len; i++ ) {
      var tempIdForGraphData='mySeries'+i;
      var tempObjForGraphData;

      tempObjForGraphData={
        // margin: {bottom: 60},
        series: [
          {
            axis: "y",
            dataset: "dataset1",
            key: parameters[i],
            label: $scope.metricLabels[parameters[i]],
            color: "#1f77b4",
            type: ['column'],
            id: tempIdForGraphData
          }
        ],
        axes: {x:
          {
            key: "_id",
            type: "date",
            ticks: object.length
          }
        }
      };
      thing.graphOption.push(tempObjForGraphData);
    }
    console.log(thing);
  }
  function generateGraphData(object,selectedParams) {
    var flattenedData=flatTheRecievedData(object,selectedParams);
    var parameters=flattenedData.pop();

    var graphData=flattenedData;
    console.log("Final Graph Data");
    console.log(graphData);
    $scope.message="";
    $scope.graphData = {
      dataset0: graphData
    };
    $scope.graphOption=[];
    console.log("parameters");
    console.log(parameters);
    for (var i = 0; i < parameters.length; i++) {
      console.log(i);
      var tempIdForGraphData='mySeries'+i;
      var tempObjForGraphData;
      console.log("temp found" + parameters[i].indexOf('temp'));
      if(parameters[i].indexOf('temp') > -1) {
        tempObjForGraphData={
          // margin: {bottom: 60},
          series: [
            {
              axis: "y",
              dataset: "dataset0",
              key: parameters[i],
              label: $scope.metricLabels[parameters[i]],
              interpolation: {mode: 'cardinal', tension: 0.7},
              color: "#1f77b4",
              type: ['line', 'dot', 'area'],
              id: tempIdForGraphData
            }
          ],
          axes: {x: {key: "time",
            type: "date",
            ticks: graphData.length/9
          },
          y: {min: 0, max:100}
          }
        };
      } else {
        tempObjForGraphData={
          // margin: {bottom: 60},
          series: [
            {
              axis: "y",
              dataset: "dataset0",
              key: parameters[i],
              label: $scope.metricLabels[parameters[i]],
              interpolation: {mode: 'cardinal', tension: 0.7},
              color: "#1f77b4",
              type: ['line', 'dot', 'area'],
              id: tempIdForGraphData
            }
          ],
          axes: {x: {key: "time",
            type: "date",
            ticks: graphData.length/9
          }
          }
        };
      }

      console.log("Temp graph data");
      console.log(tempObjForGraphData);
      $scope.graphOption.push(tempObjForGraphData);
    }
    console.log("graph option");
    console.log($scope.graphOption);

  }
  function flatTheRecievedData(object, selectedParams) {
    var objectSuitableForGraphPlot=[],
        tempArrayToStoreData;
    console.log("in function");
    console.log(object);
    for (var counter = 0; counter < object.length; counter++) {
      tempArrayToStoreData=[];
      var tempObjectToStoreData={};

      tempObjectToStoreData.time=new Date(object[counter].timestamp);
      for (var key in object[counter].data) {
        if (object[counter].data.hasOwnProperty(key) && (selectedParams===undefined || selectedParams.indexOf(key+'') > -1)) {
          if (object[counter].data[key] !== true && object[counter].data[key] !== false ) {
            tempObjectToStoreData[key]=object[counter].data[key];
            if (tempArrayToStoreData.indexOf(key) <= -1) {
              tempArrayToStoreData.push(key);
            }
          }
        }
      }

      objectSuitableForGraphPlot.push(tempObjectToStoreData);
    }
    objectSuitableForGraphPlot.push(tempArrayToStoreData);
    return objectSuitableForGraphPlot;
  }
  $scope.callApiForGettingData = function(thing,index){
    var d = new Date(),
        currentDate = $scope.getCurrentDate(d),
        yesterdayDate = $scope.getYesterdayDate(d),
        selectedParams = {
          Building: ['humidity','amb','temp'],
          Tank: ['ph_lev','tds','water_lev'],
          UPS: ['batt_lev','lonBatt','temp_ups']
        };
    $scope.currentThingName = thing.name;
    $scope.currentThingIndex = index;
    $scope.showGraphArea=true;
    $scope.isOverviewLoaded = false;
    $scope.getThingDetails(thing.thing_key);

    // Generating labels
    for(var metric in thing.metrics) {

      $scope.metricLabels[thing.metrics[metric].name] = thing.metrics[metric].abbreviation;
    }
    console.log(thing);
    var queryData = {
       "thing_key": thing.thing_key,
       "to": currentDate,
       "from": yesterdayDate,
       "time_zone":"Mumbai" ,
       "time_format":"str",
       "per":"50"
     };
     $scope.isProcessing=true;
    DashboardService.queryThingData(queryData).success(function (data) {
      $scope.isProcessing=false;
      console.log(data);
      var getupsutilization = function(data) {
        generateBarChartData(data,$scope);
      };
      for (var tempVarForKeyRunOver in data) {
        if (data.hasOwnProperty(tempVarForKeyRunOver)) {
          console.log(data[tempVarForKeyRunOver]);
          if (data[tempVarForKeyRunOver].total_event_count !== undefined) {
            if (data[tempVarForKeyRunOver].total_event_count !== 0) {

              generateGraphData(data[tempVarForKeyRunOver].event_data, selectedParams[thing.name]);
              if(thing.name == "UPS") {
                DashboardService.getUPSUtilization().success( getupsutilization);
              }
            }
            else {
              $scope.message="No data exists for this thing...";
              $scope.showGraphArea=false;
              return;
            }
          }
        } else {
              $scope.showGraphArea=false;
              $scope.message="Internal Server error Reported by Altizon";
              return;
        }
      }
    });
    $scope.showGraphArea = true;
    console.log(thing.name);
    DashboardService.queryThingTickets({"name" : thing.name}).success(
      function(data) {
        //console.log(data);
        var tickets = data.objects,
            ind = 0;
            console.log(tickets);
        $scope.tickets = [];
        for(var ticket in tickets) {
          var tickObj = tickets['' + ticket].fields,
              priority = ['critical', 'high', 'medium', 'low'],
              assignedTo = tickObj.team_id_friendlyname + '/' + tickObj.agent_id_friendlyname;
          $scope.tickets[ind] = {
            ticketNo : tickObj.friendlyname,
            sensor : tickObj.title,
            createdTime : tickObj.start_date,
            assignedTo : assignedTo.length > 2? assignedTo: '',
            status: tickObj.status,
            severity: priority[tickObj.priority-1],
            message: tickObj.description,
            parameter: ""
          };
          ind++;
        }
        $scope.displayTickets = 10;
        $scope.gridOptions.data = $scope.tickets.slice(0,$scope.displayTickets);
        $scope.sharedData.tickets = $scope.tickets;
        $scope.sharedData.currentThingName = $scope.currentThingName;
        // $scope.gridApi.core.refresh();
      }
    );
  };
  $scope.refreshThingData = function(thing, index, isSelect) {
    var selectedParams = {
          Building: ['humidity','amb'],
          Tank: ['water_lev'],
          UPS: ['batt_lev','lonBatt']
        },
        d = new Date();
    // Generating labels
    for(var metric in thing.metrics) {
      $scope.metricLabels[thing.metrics[metric].name] = thing.metrics[metric].abbreviation;
    }
    var queryData = {
       "thing_key": thing.thing_key,
       "to": $scope.getCurrentDate(d),
       "from": $scope.getYesterdayDate(d),
       "time_zone":"Mumbai" ,
       "time_format":"str",
       "per":"50"
     };
    DashboardService.queryThingData(queryData).success(function(data) {

      var event_data = data[$scope.things[index].thing_key].event_data;
      var thingdata = event_data[0].data;

      var indexMet = 0;
      for(var i in thingdata) {
        var val = thingdata['' + i];
        $scope.things[index].metrics[indexMet].latestvalue = val; // updating last event data to the things array
        indexMet++;
      }
      $scope.things[index].event_data = event_data;
      if(isSelect)
        generateGraphData(event_data, selectedParams[$scope.things[index].name]);
      else
        generateGraphData(event_data);
      $scope.things[index].graphOption = $scope.graphOption;
      $scope.things[index].graphData = $scope.graphData;
    });

  };
  generateMetricsNameArray = function(metrics) {
    return metrics.map(function(dt) {
      return dt.name;
    });
  };
  $scope.getDataForOverview = function() {
    //console.log($scope.thingData);
    //resetting
    $scope.isProcessing = true;
    $scope.tickets = $scope.thingData = $scope.graphOption = [];
    var thingPromises = [],
        d = new Date(),
        currentDate = $scope.getCurrentDate(d),
        yesterdayDate = $scope.getYesterdayDate(d),
        selectedParams = {
          Building: ['humidity','amb'],
          Tank: ['water_lev'],
          UPS: ['batt_lev','lonBatt']
        },
        tind = 0;
    $scope.panel = [];
    // deciding class to use for displaying graph.
    for(var select in selectedParams) {
      var tmp = selectedParams[select].length * 4;
      $scope.panel[tind] = {
          parent : (tmp > 12)? 'col-md-12': 'col-md-' + tmp,
          child: (tmp >= 12)? 'col-md-4': ((tmp==8)? 'col-md-6':'col-md-12')
      };
      tind++;
    }
    console.log($scope.panel);
    for(var i=0, len=$scope.things.length; i<len; i++) {
      var queryData = {
         "thing_key": $scope.things[i].thing_key,
         "to": currentDate,
         "from": yesterdayDate,
         "time_zone":"Mumbai" ,
         "time_format":"str",
         "per":"50"
       };
       console.log(queryData);
      thingPromises[i] = DashboardService.queryThingData(queryData);
    }
    $q.all(thingPromises).then(function(data) {
      $scope.graphOption=[];
      $scope.currentThingName = 'All';
      var collectiveEventData = [];
      console.log(data);
      $scope.things = data.map(function(dt,ind) {

        var event_data = dt.data[$scope.things[ind].thing_key].event_data;
        console.log($scope.things[ind].name + " " + event_data.length);
        if(event_data.length > 0) {
          var thingdata = event_data[0].data;
          console.log(thingdata);
          var metricsNameArr = generateMetricsNameArray($scope.things[ind].metrics);
          for(var i in metricsNameArr) {
            var val = metricsNameArr[i];
            //console.log(index + " " + $scope.things[ind].metrics[index].abbreviation+ " " + val);
            $scope.things[ind].metrics[i].latestvalue = thingdata[val]; // updating last event data to the things array
          }
          $scope.things[ind].event_data = event_data;
          for(var metric in $scope.things[ind].metrics) {

            $scope.metricLabels[$scope.things[ind].metrics[metric].name] = $scope.things[ind].metrics[metric].abbreviation;
          }
          //console.log(event_data);
          generateGraphData(event_data, selectedParams[$scope.things[ind].name]);
          $scope.things[ind].graphOption = $scope.graphOption;
          $scope.things[ind].graphData = $scope.graphData;
          if($scope.things[ind].name == "UPS") {
            DashboardService.getUPSUtilization().success( function(data) {
              generateBarChartData(data,$scope.things[ind]);
            });
          }
        }
        return $scope.things[ind];
      });

      Ticketservice.queryThingTickets({name : 'All Tickets'}).success(
        function(data) {
          //console.log(data);
          var tickets = data.objects,
              ind = 0;
          $scope.tickets = [];
          for(var ticket in tickets) {
            var tickObj = tickets['' + ticket].fields,
                priority = ['critical', 'high', 'medium', 'low'],
                assignedTo = tickObj.team_id_friendlyname + '/' + tickObj.agent_id_friendlyname;
            $scope.tickets[ind] = {
              ticketNo : tickObj.friendlyname,
              sensor : tickObj.title,
              createdTime : tickObj.start_date,
              assignedTo : assignedTo.length > 2? assignedTo: '',
              status: tickObj.status,
              severity: priority[tickObj.priority-1],
              message: tickObj.description,
              parameter: ""
            };
            ind++;
          }
          $scope.tickets.sort(function(a, b) {
            return Date.parse(b.createdTime) - Date.parse(a.createdTime);
          });
          $scope.displayTickets = 5;
          $scope.gridOptions.data = $scope.tickets.slice(0,$scope.displayTickets);
          $scope.isProcessing = false;
        }
      );
      $scope.showGraphArea = true;
      $scope.isProcessing = false;
      $scope.isOverviewLoaded = true;
    });

  };
  $scope.isOverviewLoaded = false;
  $scope.getDataForOverview();

});
