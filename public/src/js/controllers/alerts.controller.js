
angular.module('altizonApp').controller('AlertsCtrl', function ($scope, $rootScope, DashboardService, $filter) {
  $scope.sharedData.updateActiveTab(1); // updating active tab
  
  $scope.gridOptions = {
    enableSorting: true,
    enableVerticalScrollbar: 1,
    enablePaginationControls: false,
    columnDefs: [
      { field : 'time', enableColumnResizing: true, width: '30%' },
      { field : 'thing', enableColumnResizing: true, width: '20%' },
      { field : 'type', enableColumnResizing: true, width: '10%' },
      { field : 'message', enableColumnResizing: true, width: '40%' }
    ],
    enableHorizontalScrollbar:  0
  };

  /*$scope.getFormatedDate = function(tdate) {
    var date = new Date(tdate);
    return $filter('date')(tdate, 'yyyy/MM/dd hh:mm:ss');
  };*/

  $scope.pagination = {
       pageSize: '10',
       pageNumber: '1',
       totalItems: null,
       getTotalPages: function () {
           return Math.ceil(this.totalItems / this.pageSize);
       },
       nextPage: function () {
           if (this.pageNumber < this.getTotalPages()) {
               this.pageNumber++;
               $scope.load();
           }
       },
       previousPage: function () {
           if (this.pageNumber > 1) {
               this.pageNumber--;
               $scope.load();
           }
       },
       changePageSize: function() {
         this.getTotalPages();
         $scope.load();
       }
   };

   /*$scope.checkAlertType = function(alertType, position) {
     console.log(alertType);
     console.log($scope.gridOptions.columnDefs[2].cellTemplate);
     if(alertType === 'Info' && $scope.gridOptions.columnDefs[2].field === 'type') {
       $scope.gridOptions.columnDefs[2].cellTemplate = '<button class="btn btn-primary">Info</button>';
     }else if(alertType === 'Warning' && $scope.gridOptions.columnDefs[2].field === 'type'){
       $scope.gridOptions.columnDefs[2].cellTemplate = '<button type="button" class="btn btn-warning">Warning</button>';
     }
   };*/

   $scope.load = function () {
        $scope.isProcessing = true;
        $scope.showingMessage = false;
        DashboardService.getAllAlerts($scope.pagination.pageSize, $scope.pagination.pageNumber).then(function (response) {
          var alertsDetails = response.data.alertData;
          if(alertsDetails){
            var alerts = alertsDetails.alerts;
            var count = 0;
            $scope.alertsData = [];
            for(var alert in alerts) {
              $scope.alertsData[count] = {
                time : $filter('date')(alerts[alert].created_at, 'yyyy/MM/dd hh:mm:ss'),
                thing : alerts[alert].thing_name,
                type : alerts[alert].alert_type_string,
                message : alerts[alert].message
              };
              count++;
            }
            $scope.gridOptions.data = $scope.alertsData;
            $scope.pagination.totalItems = alertsDetails.total_count;
          } else {
            $scope.showingMessage = true;
            $scope.noData = 'No data found!!';
          }
          $scope.isProcessing = false;
        });
    };

$scope.load();
});
