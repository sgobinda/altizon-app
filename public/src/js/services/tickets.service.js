angular.module('altizonApp').factory('Ticketservice', function ($http, $window) {
  return {
    queryThingTickets: function(queryData) {
      console.log(queryData);
      var q = queryData;

      var req = {};
      req.url = (q.name.indexOf('All') > -1)? '/user/getAllTickets':'/user/getThingTickets';
      req.method = 'POST';
      req.data = queryData;
      //req.params.data = data;
      return $http(req);
    }
  };
});
