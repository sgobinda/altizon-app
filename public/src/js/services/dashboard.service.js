angular.module('altizonApp').factory('DashboardService', function ($http, $window) {
  return {

    getAllThings: function () {
      var req = {};
      req.url = '/user/thing/getAllThings';
      req.method = 'GET';
      return $http(req).success(function (response) {
        return response.data;
      });
    },

    getThingDetails: function (thingKey) {
      var req = {};
      req.url = '/user/thing/getThingDetails/'+thingKey;
      req.method = 'GET';
      return $http(req);
    },

    queryThingData: function (queryData) {
      var req = {};
      req.url = '/user/thing/queryThingData';
      req.method = 'POST';
      req.data = queryData;
      return $http(req);
    },

    queryThingTickets: function(queryData) {
      var req = {};
      req.url = '/user/getThingTickets';
      req.method = 'POST';
      req.data = queryData;
      //req.params.data = data;
      return $http(req);
    },

    getAllAlerts: function (pageSize, pageNumber) {
      var req = {};
      req.method = 'GET';
      req.url = '/user/thing/getAllAlerts';
      req.params = {};
      req.params.per = pageSize;
      req.params.pageNumber = pageNumber;
      return $http(req).success(function (response) {
        return response.data;
      });
    },

    getAlertNotifications: function () {
      var req = {};
      req.method = 'GET';
      req.url = '/user/thing/getAlertNotifications';
      return $http(req).success(function (response) {
        return response.data;
      });
    },

    getUPSUtilization: function() {
      var req = {};
      req.method = 'GET';
      req.url = '/upsUtilization';
      return $http(req).success(function(response) {
        return response.data;
      });
    }

  };
});
