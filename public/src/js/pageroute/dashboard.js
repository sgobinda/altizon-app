
angular.module('altizonApp').config(function ($stateProvider) {
  $stateProvider.state('home', {
    'abstract' : false,
    parent: 'site',
    url: '/home',
    access: { requiredAuthentication: true },
    views: {
      'content@': {
        templateUrl: 'views/pages/home.html',
        controller: 'homeCtrl'
      }
    }
  })
  .state('home.dashboard', {
    parent: 'home',
    url: '/dashboard',
    access: { requiredAuthentication: true },
    views: {
      'pagesContain': {
        templateUrl: 'views/pages/dashboard.html',
        controller: 'DashboardCtrl'
      }
    },
    resolve: {
      getThings: function(DashboardService) {
        return DashboardService.getAllThings().then(function (response) {
          return response.data;
        });
      }
    }
  })
  .state('home.alerts', {
    parent: 'home',
    url: '/alerts',
    access: { requiredAuthentication: true },
    views: {
      'pagesContain': {
        templateUrl: 'views/pages/alerts.html',
        controller: 'AlertsCtrl'
      }
    }
  })
  .state('home.tickets', {
    parent: 'home',
    url: '/tickets',
    access: { requiredAuthentication: true },
    views: {
      'pagesContain': {
        templateUrl: 'views/pages/tickets.html',
        controller: 'TicketsCtrl'
      }
    }
  });
});
